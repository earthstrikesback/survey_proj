<ogr:FeatureCollection xsi:schemaLocation="" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogr="http://ogr.maptools.org/" xmlns:gml="http://www.opengis.net/gml">
   <gml:boundedBy>
      <gml:Box>
         <gml:coord>
            <gml:X>22265.44963750511</gml:X>
            <gml:Y>21162.98653703742</gml:Y>
         </gml:coord>
         <gml:coord>
            <gml:X>295157.4067411629</gml:X>
            <gml:Y>244027.902640108</gml:Y>
         </gml:coord>
      </gml:Box>
   </gml:boundedBy>
   <gml:featureMember>
      <ogr:zoning fid="31372691">
         <ogr:zoningName>W04-C-Evere-FH05</ogr:zoningName>
         <ogr:zoningId>32939</ogr:zoningId>
         <ogr:technologyCode>FTTH-Cu</ogr:technologyCode>
         <ogr:typeCode>NA</ogr:typeCode>
         <ogr:layoutCode>NA</ogr:layoutCode>
         <ogr:phase></ogr:phase>
         <ogr:comment></ogr:comment>
         <ogr:geometryProperty>
         <gml:Polygon srsName="EPSG:31370" xmlns:gml="http://www.opengis.net/gml"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">153715.816143612 172272.712352863 0.0 154020.66195859 172398.703542291 0.0 154080.573852863 172504.430414537 0.0 153831.234645815 172819.84891674 0.0 153477.049623789 172627.778432159 0.0 153483.21702467 172603.989885903 0.0 153697.313940969 172282.403982819 0.0 153715.816143612 172272.712352863 0.0 </gml:posList></gml:LinearRing></gml:exterior></gml:Polygon>        </ogr:geometryProperty>
      </ogr:zoning>
   </gml:featureMember>
</ogr:FeatureCollection>