<ogr:FeatureCollection xsi:schemaLocation="" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogr="http://ogr.maptools.org/" xmlns:gml="http://www.opengis.net/gml">
   <gml:boundedBy>
      <gml:Box>
         <gml:coord>
            <gml:X>22265.44963750511</gml:X>
            <gml:Y>21162.98653703742</gml:Y>
         </gml:coord>
         <gml:coord>
            <gml:X>295157.4067411629</gml:X>
            <gml:Y>244027.902640108</gml:Y>
         </gml:coord>
      </gml:Box>
   </gml:boundedBy>
   <gml:featureMember>
      <ogr:zoning fid="21066466">
         <ogr:zoningName>W03-C-Schaerbeek-FH10</ogr:zoningName>
         <ogr:zoningId>30634</ogr:zoningId>
         <ogr:technologyCode>FTTH-Cu</ogr:technologyCode>
         <ogr:typeCode>RESCOR</ogr:typeCode>
         <ogr:layoutCode>MIX</ogr:layoutCode>
         <ogr:phase></ogr:phase>
         <ogr:comment></ogr:comment>
         <ogr:geometryProperty>
         <gml:Polygon srsName="EPSG:31370" xmlns:gml="http://www.opengis.net/gml"><gml:exterior><gml:LinearRing><gml:posList srsDimension="3">150824.51499865 172039.927841958 0.0 150813.088879286 172022.294941703 0.0 150725.065441217 171845.965939162 0.0 150704.47021372 171807.314621805 0.0 150683.310733415 171757.378248286 0.0 150665.254643555 171695.874692199 0.0 150683.310733415 171682.896877612 0.0 150674.423751687 171665.687166964 0.0 150671.461424444 171653.414668387 0.0 150667.793781192 171618.007804677 0.0 150666.524212373 171587.397089836 0.0 150669.909729222 171523.354396113 0.0 150677.809268536 171479.624803483 0.0 150689.376451103 171435.04883164 0.0 150695.018979184 171418.4033738 0.0 150711.38231062 171379.469930039 0.0 150735.080928561 171335.176084601 0.0 150768.089717837 171281.289941424 0.0 150784.453049273 171290.882239163 0.0 150831.286032348 171207.372823559 0.0 150869.090970493 171143.048003432 0.0 151325.571492271 171332.636946964 0.0 150824.51499865 172039.927841958 0.0 </gml:posList></gml:LinearRing></gml:exterior></gml:Polygon>        </ogr:geometryProperty>
      </ogr:zoning>
   </gml:featureMember>
</ogr:FeatureCollection>